/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ToolbarAndroid,
  ListView
} from 'react-native';
import * as firebase from 'firebase';
import ListItem from './src/listitem.js';
import styles from './styles.js'

export default class Firebase extends Component {

    constructor(props) {
        super(props);
        
        const dataSource = new ListView.DataSource({rowHasChanged: (row1, row2) => row1 !== row2, });
    
        this.state = { dataSource: dataSource.cloneWithRows([
            { name: 'Sleep' }, { name: 'Eat' }, { name: 'Code' },
            { name: 'Sleep' }, { name: 'Eat' }, { name: 'Code' },
            { name: 'Sleep' }, { name: 'Eat' }, { name: 'Code' },
            { name: 'Sleep' }, { name: 'Eat' }, { name: 'Code' }])
        };
    }

    _renderItem(task) {
        return (
            <ListItem task={task} />
        );
    }

    render() {
      return (
          <View style={styles.container}>
            <ToolbarAndroid
                style={styles.navbar}
                title="Todo List" />
            
            <ListView
                enableEmptySections={true}
                dataSource={this.state.dataSource}
                renderRow={this._renderItem.bind(this)}
                style={styles.listView}/>
          </View>
      );
    }
}

/*
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f2f2f2',
    flex: 1,
  },
  listView: {
    flex: 1,
  },
  listItem: {
    borderBottomColor: '#eee',
    borderColor: 'gray',
    flexDirection:'row',
    alignItems:'center',
    borderWidth: 1,
    padding:20
  },
  listItemTitle: {
    flex: 6,
    color: '#000',
    fontSize: 16,
  },
  listItemAction: {
    flex: 1,
    width: 40,
    height: 40
  },
  navbar: {
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomColor: '#eee',
    borderColor: 'transparent',
    borderWidth: 1,
    justifyContent: 'center',
    height: 54,
    flexDirection: 'row'
  },
  navbarTitle: {
    color: '#444',
    fontSize: 16,
    fontWeight: "500"
  }
});
*/

AppRegistry.registerComponent('Firebase', () => Firebase);
